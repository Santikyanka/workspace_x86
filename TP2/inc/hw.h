#ifndef _HW_H_
#define _HW_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

#define EXIT      27  // ASCII para la tecla Esc
#define SENSOR_FICHA  49  // ASCII para la tecla 1
#define SENSOR_TE  50  // ASCII para la tecla 2
#define SENSOR_CAFE  51  // ASCII para la tecla 3

/*==================[typedef]================================================*/

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Funciones que configuran la consola de Linux como interfaz de I/O
void hw_Init(void);
void hw_DeInit(void);

// Funciones basicas para leer que entrada esta activa y pausar la ejecucion
void hw_Pausems(uint16_t t);
uint8_t hw_LeerEntrada(void);

void hw_Devolver_ficha();
void hw_te_on();
void hw_te_off();
void hw_cafe_on();
void hw_cafe_off();
void hw_sonido_on();
void hw_sonido_off();

/*==================[end of file]============================================*/
#endif /* #ifndef _HW_H_ */

#ifndef _EXPENDEDORA_H_
#define _EXPENDEDORA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    INSERT_COIN,
    SELECT_DRINK,
    EROG_TE,
    EROG_CAFE,
    SONIDO
} FSM_EXPENDEDORA_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_expendedora_init(void);
void fsm_expendedora_runCycle(void);

// Eventos
void fsm_expendedora_raise_evIngresoFicha(void);
void fsm_expendedora_raise_evElige_te(void);
void fsm_expendedora_raise_evElige_cafe(void);
void fsm_expendedora_raise_evTick1seg(void);
/*void fsm_expendedora_raise_evTickLEDms(void);*/ //led ver

// Debugging
void fsm_expendedora_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _EXPENDEDORA_H_ */

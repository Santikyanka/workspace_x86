/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_expendedora.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;

    hw_Init();

    fsm_expendedora_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_FICHA) {
            fsm_expendedora_raise_evIngresoFicha();
        }

        if (input == SENSOR_TE) {
            fsm_expendedora_raise_evElige_te();
        }

        if (input == SENSOR_CAFE) {
            fsm_expendedora_raise_evElige_cafe();
        }
       
        cont_ms++;
        if (cont_ms == 1000) {
            cont_ms = 0;
            fsm_expendedora_raise_evTick1seg();
        }

        fsm_expendedora_runCycle(); 

        hw_Pausems(1);
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/

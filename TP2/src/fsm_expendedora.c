/*==================[inclusions]=============================================*/

#include "fsm_expendedora.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_SEL_DRINK_SEG  3
#define ESPERA_TE_SEG  3
#define ESPERA_CAFE_SEG  5
#define ESPERA_SONIDO_SEG 2

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

FSM_EXPENDEDORA_STATES_T state;

bool evIngresoFicha_raised;
bool evElige_te_raised;
bool evElige_cafe_raised;
bool evTick1seg_raised;
uint8_t count_seg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evIngresoFicha_raised = 0;
    evElige_te_raised = 0;
    evElige_cafe_raised = 0;
    evTick1seg_raised = 0;
}

/*==================[external functions definition]==========================*/

void fsm_expendedora_init(void)
{
    state = INSERT_COIN;
    clearEvents();
}

void fsm_expendedora_runCycle(void)
{
    // El diagrama se encuentra en fsm_expendedora/media/
    switch (state) {
        case INSERT_COIN:
            if (evIngresoFicha_raised) {
                count_seg = 0;
                state = SELECT_DRINK;
            }
            break;

        case SELECT_DRINK:
             if (evTick1seg_raised && (count_seg < ESPERA_SEL_DRINK_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_SEL_DRINK_SEG)) {
                hw_Devolver_ficha();
                state = INSERT_COIN;
            }
            else if (evElige_te_raised) {
                count_seg = 0;
                hw_te_on();
                state = EROG_TE;
            }
            else if (evElige_cafe_raised) {
                count_seg = 0;
                hw_cafe_on();
                state = EROG_CAFE;
            }
            break;

        case EROG_TE:
            if (evTick1seg_raised && (count_seg < ESPERA_TE_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_TE_SEG)) {
                hw_te_off();
                hw_sonido_on();
                count_seg = 0;
                state = SONIDO;
            }
            break;

        case EROG_CAFE:
            if (evTick1seg_raised && (count_seg < ESPERA_CAFE_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_CAFE_SEG)) {
                hw_cafe_off();
                hw_sonido_on();
                count_seg = 0;
                state = SONIDO;
            }
            break;

        case SONIDO:
            if (evTick1seg_raised && (count_seg < ESPERA_SONIDO_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_SONIDO_SEG)) {
                hw_sonido_off();
                state = INSERT_COIN;
            }
            break;
    }

    clearEvents();
}

void fsm_expendedora_raise_evIngresoFicha(void)
{
    evIngresoFicha_raised = 1;
    printf("Ficha ingresada, elija su bebida \n\n");

}

void fsm_expendedora_raise_evElige_te(void)
{
    evElige_te_raised = 1;
    printf("Usted eligio te, espere 3 segundos \n\n");
}

void fsm_expendedora_raise_evElige_cafe(void)
{
    evElige_cafe_raised = 1;
    printf("Usted eligio te, espere 5 segundos \n\n");
}

void fsm_expendedora_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_expendedora_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
}

/*==================[end of file]============================================*/

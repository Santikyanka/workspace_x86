/*==================[inclusions]=============================================*/

#include "hw.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

struct termios oldt, newt;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

void hw_Init(void)
{
    // Configurar la terminal para evitar presionar Enter usando getchar()
    tcgetattr(STDIN_FILENO, &oldt);
    newt = oldt;
    newt.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(STDIN_FILENO, TCSANOW, &newt);

    // Non-blocking input
    fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

void hw_DeInit(void)
{
    // Restaurar la configuracion original de la terminal
    tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

void hw_Pausems(uint16_t t)
{
    usleep(t * 1000);
}

uint8_t hw_LeerEntrada(void)
{
    return getchar();
}

void hw_Devolver_ficha(void)
{
    printf("Devolviendo ficha \n\n");
}

void hw_te_on(void)
{
    printf("Haciendo Te \n\n");
}

void hw_cafe_on(void)
{
    printf("Haciendo Cafe \n\n");
}

void hw_te_off(void)
{
    printf("Listo su Te \n\n");
}

void hw_cafe_off(void)
{
    printf("Listo su Cafe \n\n");
}

void hw_sonido_on(void)
{
    printf("UUUUUUUUUUUUUUUU \n\n");
}

void hw_sonido_off(void)
{
    printf("*Silencio* \n\n");
}

/*==================[end of file]============================================*/

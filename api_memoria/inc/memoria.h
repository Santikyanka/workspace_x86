#ifndef _MEMORIA_H_
#define _MEMORIA_H_

/*==================[inclusions]=============================================*/

#include <stdint.h>

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef struct {
    uint8_t slaveAddr;  /**< Slave address */
    uint16_t addr;      /**< Direccion inicial de la memoria a escribir/leer */
    uint8_t *txBuff;    /**< Puntero al vector de bytes a transmitir */
    uint16_t txSz;      /**< Cantidad de bytes a transmitir, 0 para recibir */
    uint8_t *rxBuff;    /**< Puntero donde se deben almacenar los bytes recibidos */
    uint16_t rxSz;      /**< Cantidad de bytes a recibir, 0 para transmitir */
    int16_t status;     /**< Estado de la transferencia */
} MEM_XFER_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

int16_t memRead(uint8_t id, uint16_t addr, void *data, uint16_t len);
int16_t memWrite(uint8_t id, uint16_t addr, void *data, uint16_t len);

/*==================[end of file]============================================*/
#endif /* #ifndef _MEMORIA_H_ */
